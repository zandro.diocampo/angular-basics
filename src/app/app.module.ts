import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeroesListComponent } from './Components/heroes-list/heroes-list.component';
import { ItemOutputComponent } from './Components/item-output/item-output.component';

@NgModule({
  declarations: [
    AppComponent,
    HeroesListComponent,
    ItemOutputComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
